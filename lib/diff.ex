defmodule Diff do
  @moduledoc """
  Provides a modified Myers diff algorithm suitable for syncing messages
  in channels. Why do we need this? The traditional Myers diff algorithm
  only supports addition and deletion, and we are also capable of editing
  messages. Additionally, the traditional Myers diff algorithm will insert
  in the middle of a string, however our only method of adding messages
  appends them to the channel. Therefore, we make 2 modifications: when
  constructing the grid, we connect every node "diagonally" (representing an edit)
  and only connect nodes "vertically" (representing an insertion) on the
  right-most edge of the grid (At the end of the channel). The end result
  is as follows (edges with a "0" on them represent 0 weight edges):

           A     B     C     A     B     B     A

        o-----o-----o-----o-----o-----o-----o-----o   0
          \     \     \     \     \     \     \   |
    C      \     \     0     \     \     \     \  |
            \     \     \     \     \     \     \ |
        o-----o-----o-----o-----o-----o-----o-----o   1
          \     \     \     \     \     \     \   |
    B      \     0     \     \     0     0     \  |
            \     \     \     \     \     \     \ |
        o-----o-----o-----o-----o-----o-----o-----o   2
          \     \     \     \     \     \     \   |
    A      0     \     \     0     \     \     0  |
            \     \     \     \     \     \     \ |
        o-----o-----o-----o-----o-----o-----o-----o   3
          \     \     \     \     \     \     \   |
    B      \     0     \     \     0     0     \  |
            \     \     \     \     \     \     \ |
        o-----o-----o-----o-----o-----o-----o-----o   4
          \     \     \     \     \     \     \   |
    A      0     \     \     0     \     \     0  |
            \     \     \     \     \     \     \ |
        o-----o-----o-----o-----o-----o-----o-----o   5
          \     \     \     \     \     \     \   |
    C      \     \     0     \     \     \     \  |
            \     \     \     \     \     \     \ |
        o-----o-----o-----o-----o-----o-----o-----o   6

        0     1     2     3     4     5     6     7
  """
  def diff(from, to) do
    diff(from, to, &(&1 == &2))
  end
  
  def diff(from, to, by) do
    dims = {Enum.count(from), Enum.count(to)}
    make_edges(from, to, by)
    |> Graph.a_star({0, 0}, dims, fn _ -> 0 end)
    |> directions(from, to, by)
  end

  def make_edges(from, to, by) do
    len = Enum.count(from)
    Stream.with_index(to)
    |> Enum.reduce(Graph.new(), fn({y, iy}, g) ->
      Stream.with_index(from)
      |> Enum.reduce(g, fn({x, ix}, g) ->
        Graph.add_edge(g, {ix, iy}, {ix + 1, iy + 1}, weight: weight(x, y, by))
        |> Graph.add_edge({ix, iy}, {ix + 1, iy})
      end)
      |> Graph.add_edge({len, iy}, {len, iy + 1})
    end)
  end

  def directions(stream, from, to, by) do
    Stream.transform(stream, nil, fn(x, last) -> {[{last, x}], x} end)
    |> Stream.drop(1)
    |> Stream.map(fn({{x1, y1}, {x2, y2}}) ->
      if x1 + 1 == x2 do
        if y1 + 1 == y2 do
          if by.(Enum.at(from, x1), Enum.at(to, y1)) do
            :nop
          else
            {:edit, Enum.at(from, x1), Enum.at(to, y1)}
          end
        else
          {:delete, Enum.at(from, x1)}
        end
      else
        {:insert, Enum.at(to, y1)}
      end
    end)
  end

  defp weight(a, b, by) do
    if by.(a, b) do
      0
    else
      1
    end
  end
end
